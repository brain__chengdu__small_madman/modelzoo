# ModelArts云上训练指引

- [创建OBS桶](#1)

- [创建算法](#2)

- [创建训练作业](#3)

- [查看训练任务日志](#4)

  

## <span id='1'>创建OBS桶</span>

1. 登录[OBS管理控制台](https://storage.huaweicloud.com/obs/?region=cn-north-4#/obs/manager/buckets "OBS管理控制台")，创建OBS桶。具体请参见[创建桶](https://support.huaweicloud.com/usermanual-obs/obs_03_0306.html "创建桶")章节。如[图1 创建桶](#6)所示：<br>
    <font size=1>**图1** 创建桶</font><br>
    <img src="../images/ModelArts云上训练指引/创建桶.png#" alt="创建桶" style="zoom:60%;" /><br>

    > **说明：** <br>
    >创建桶的区域需要与ModelArts所在的区域一致。<br>
    >例如，当前ModelArts在华北-北京四区域，在对象存储服务创建桶时，请选择华北-北京四。<br>

2. 创建用于存放数据的文件夹，具体请参见[新建文件夹](https://support.huaweicloud.com/usermanual-obs/obs_03_0316.html "新建文件夹")章节。如[图2 obs桶目录结构](#7)所示：<br>
    <font size=1>**图2** obs桶目录结构</font><br>
    <img src="../images/ModelArts云上训练指引/obs桶目录.png" alt="obs桶目录" style="zoom:50%;" /><br>
    >**目录结构说明：**<br>
    >- code：存放训练脚本目录。<br>
    >- data：存放训练数据集目录。<br>
    >- logs：存放训练日志目录。<br>
    >- output：存放训练生成ckpt和air模型目录。<br>
    >- preckpt：存放预训练模型目录。<br>

3. 将代码包文件夹上传至“code”目录，数据集上传至“data”目录，若有预训练模型上传至“preckpt”目录。<br>




## <span id='2'>创建算法</span>

1. 使用华为云帐号登录[ModelArts管理控制台](https://console.huaweicloud.com/modelarts/?region=cn-north-4 "ModelArts管理控制台")，在左侧导航栏中选择“算法管理”。如[图3 算法管理](#8)所示：<br>
    <font size=1>**图3** 算法管理</font><br>
    <img src="../images/ModelArts云上训练指引/算法管理.png" alt="算法管理" style="zoom:67%;" /><br>

2. 在“我的算法”界面，单击左上角“创建”，进入“创建算法”页面。如[图4 创建算法](#9)所示：<br>
    <font size=1>**图4** 创建算法</font><br>
    <img src="../images/ModelArts云上训练指引/创建算法.png" alt="创建算法" style="zoom:67%;" /><br>

3. 在“创建算法”页面，填写相关参数，然后单击“提交”。如[图5 填写算法相关参数](#10)所示：<br>
    <font size=1>**图5** 填写算法相关参数</font><br>
    <img src="../images/ModelArts云上训练指引/创建算法1.png" alt="创建算法1" style="zoom:100%;" /><br>
    >**说明：**<br>
    >- 设置算法基本信息。<br>
    >- 设置“创建方式”为“自定义脚本”。<br>
    >- 用户需根据实际算法代码情况设置“AI引擎”、“代码目录”和“启动文件”。选择的AI引擎和编写算法代码时选择的框架必须一致。例如，编写算法代码使用的是MindSpore，则在创建算法时也要选择MindSpore。<br>
    >- 单击“增加超参”，手动添加超参。配置代码中的命令行参数值，请根据您编写的算法代码逻辑进行填写，确保参数名称和代码的参数名称保持一致。可填写多个参数。<br>

   <font size=2>**表 1** 创建算法参数表</font>

    | 参数名称     | 说明                                                         |
    | ------------ | ------------------------------------------------------------ |
    | AI引擎       | Ascend-Powered-Engine，mindspore_1.6.0-cann_5.0.4            |
    | 代码目录     | 算法代码存储的OBS路径，如“/obs桶名称/mindspore-dataset/code/YOLOv4_for_MindSpore\_{version}_code” |
    | 启动文件     | 启动训练的Python脚本，如“/obs桶名称/mindspore-dataset/code/YOLOv4_for_MindSpore_{version}_code/modelarts.py”。<br><strong>须知：</strong>需要把“modelart/”目录下的modelarts.py启动脚本拷贝到根目录下。 |
    | 输入数据配置 | 代码路径参数：“data_url”                                      |
    | 输入数据配置 | 代码路径参数：“checkpoint_url”                                |
    | 输出数据配置 | 代码路径参数：“train_url”                                    |




## <span id='3'>创建训练作业</span>

1. 使用华为云帐号登录[ModelArts管理控制台](https://console.huaweicloud.com/modelarts/?region=cn-north-4 "ModelArts管理控制台")，在左侧导航栏中选择“训练管理” > “训练作业”，默认进入“训练作业”列表。如[图6 训练作业](#11)所示：<br>
    <font size=1>**图6** 训练作业</font><br>
    <img src="../images/ModelArts云上训练指引/训练管理.png" alt="训练管理" style="zoom:67%;" /><br>

2. 在“训练作业”界面，单击右上角“创建训练作业”，进入“创建训练作业”页面。如[图7 创建训练作业](#12)所示：<br>
    <font size=1>**图7** 创建训练作业</font><br>
    <img src="../images/ModelArts云上训练指引/创建训练作业.png" alt="创建训练作业" style="zoom:67%;" /><br>

3. 在“创建训练作业”页面，填写训练作业相关参数，然后单击“提交”。如[图8 填写训练作业相关参数](#12)所示：<br>
    <font size=1>**图8** 填写训练作业相关参数</font><br>
    <img src="../images/ModelArts云上训练指引/创建训练作业1.png" alt="创建训练作业1" style="zoom:80%;" /><br>
    >**说明：**<br>
    >- 填写基本信息, 包含“名称”和“描述”。<br>
    >- 填写作业参数，包含数据来源、算法来源等关键信息。本步骤只提供训练任务部分参数配置说明，其他参数配置详情请参见《[ModelArts AI 工程师用户指南](https://support.huaweicloud.com/modelarts/index.html "ModelArts AI 工程师用户指南")》中“训练模型”。<br>
    >- 单击“提交”，完成训练作业的创建。<br>
    >- 训练作业一般需要运行一段时间，根据您选择的数据量和资源不同，训练时间将耗时几分钟到几十分钟不等。<br>

    <font size=2>**表 2**  创建训练作业参数表</font>

    |参数名称     |子参数     |说明   |
    |---          |---       |---     |
    |算法         |我的算法   |选择“我的算法”页签，勾选上文中创建的算法。如果没有创建算法，请单击“创建”进入创建算法页面，详细操作指导参见“创建算法”。|
    |训练输入     |数据来源      |选择OBS上数据集存放的目录。|
    |训练输出     |模型输出      |选择训练结果的存储位置（OBS路径），请尽量选择空目录来作为训练输出路径。 |
    |规格         |-            |Ascend: 1*Ascend 910(32GB) &#124; ARM: 24 核 96GB  |
    |作业日志路径   |-     |设置训练日志存放的目录。请注意选择的OBS目录有读写权限。|




## <span id='4'>查看训练任务日志</span>

1. 在ModelArts管理控制台，在左侧导航栏中选择“训练管理” > “训练作业”，默认进入“训练作业”列表。如[图9 训练作业列表](#13)所示：<br>
    <font size=1>**图9** 训练作业列表</font><br>
    <img src="../images/ModelArts云上训练指引/训练作业列表.png" alt="训练作业列表" style="zoom: 67%;" /><br>

2. 在训练作业列表中，您可以单击作业名称，查看该作业的详情。
- 详情中包含作业的基本信息、训练参数、日志详情和资源占用情况。如[图10 检查训练作业日志](#14)所示：<br>
    <font size=1>**图10** 检查训练作业日志</font><br>
    <img src="../images/ModelArts云上训练指引/日志.png" alt="日志" style="zoom: 50%;" /><br>

- 成功生成air模型。如[图11 检查保存的air模型](#15)所示：<br>
    <font size=1>**图11** 检查保存的air模型</font><br>
    <img src="../images/ModelArts云上训练指引/ckpt_air1.png" alt="ckpt_air1" style="zoom: 50%;" /><br>

- 成功生成ckpt模型。如[图12 检查保存的ckpt模型](#16)所示：<br>
    <font size=1>**图12** 检查保存的ckpt模型</font><br>
    <img src="../images/ModelArts云上训练指引/ckpt_air2.png" alt="ckpt_air2" style="zoom: 50%;" /><br>

- 成功保存训练日志。如[图13 检查保存的训练日志](#17)所示：<br>
    <font size=1>**图13** 检查保存的训练日志</font><br>
    <img src="../images/ModelArts云上训练指引/保存训练日志.png" alt="保存训练日志" style="zoom:50%;" /><br>
